<#assign base=request.contextPath />
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="蜗牛库 - 解答咨询问题。">
    <meta name="author" content="蜗牛库">
    <meta name="keyword" content="源码, 免费下载, 在线演示, 技术支持, 咨询服务">

    <title>评分  - 蜗牛库</title>
    <link href="${base}/assets/img/logo.ico" rel="shortcut icon">

    <!-- Bootstrap core CSS -->
    <link href="${base}/assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="${base}/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        
    <!-- Custom styles for this template -->
    <link href="${base}/assets/css/style.css" rel="stylesheet">
    <link href="${base}/assets/css/style-responsive.css" rel="stylesheet">
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  
  <#include "public/index/top.ftl">
  <#include "public/index/left.ftl">
  <#include "public/star/main.ftl">
  <#include "public/index/modal.ftl">
  
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="${base}/assets/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="${base}/assets/js/jquery.dcjqaccordion.2.7.js"></script>
  <script src="${base}/assets/js/jquery.scrollTo.min.js"></script>
  <script src="${base}/assets/js/jquery.nicescroll.js" type="text/javascript"></script>

  <!--common script for all pages-->
  <script src="${base}/assets/js/common-scripts.js"></script>
  
  <script type="text/javascript" src="${base}/star/lib/jquery.raty.min.js"></script>
  <script>
    	$(document).ready(function(){
			var id = $("#id").val();
			var allData = {
　　　　　　　　　　  	id:id
　　　　　　　　   };
	        $.ajax({
               url:'${base}/resource/getQuestionById',
               type:'post',
               contentType:'application/json;charset=UTF-8',
			   dataType:'json',
               data:JSON.stringify(allData),
               success:function(data){
               		if(data.status==0){
           				alert(data.message);
               		}else if(data.status==1){
	                    $("#fuwu").html(data.money+"的"+data.title);
	                    $("#resource").html(data.resource);
               		}
               },
               error:function(){
					alert("服务器错误！请联系站长");
               }
	         });
	         
	         $(function() {
			      $.fn.raty.defaults.path = '${base}/star/lib/img';
			      $('#function-demo').raty({
				  	number: 5,//多少个星星设置		
					targetType: 'hint',//类型选择，number是数字值，hint，是设置的数组值
			        path      : '${base}/star/demo/img',
					hints     : ['非常不好','很不好','一般','很好','非常好'],
			        cancelOff : 'cancel-off-big.png',
			        cancelOn  : 'cancel-on-big.png',
			        size      : 24,
			        starHalf  : 'star-half-big.png',
			        starOff   : 'star-off-big.png',
			        starOn    : 'star-on-big.png',
			        target    : '#function-hint',
			        cancel    : false,
			        targetKeep: true,
					targetText: '请选择评分',
			
			        click: function(score, evt) {
				        if(confirm("确定"+score+"星评分?")){
				        	var id = $("#id").val();
				        	var allData = {
				　　　　　　　　　　  	id:id,
								score:score
				　　　　　　　　   };
					        $.ajax({
				               url:'${base}/resource/star',
				               type:'post',
				               contentType:'application/json;charset=UTF-8',
							   dataType:'json',
				               data:JSON.stringify(allData),
				               success:function(data){
				               		alert(data.message);
				               		if(data.status==1){
				               			window.location.href = "${base}/resource/"+data.id+"_new.html";
				               		}
				               },
				               error:function(){
									alert("服务器错误！请联系站长");
				               }
					         });
						}
			        }
			      });    
			    });
		});
  </script>
  </body>
</html>
