<#assign base=request.contextPath />
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="蜗牛库 - 解答咨询问题。">
    <meta name="author" content="蜗牛库">
    <meta name="keyword" content="源码, 免费下载, 在线演示, 技术支持, 咨询服务">

    <title>赞助网站  - 蜗牛库</title>
    <link href="${base}/assets/img/logo.ico" rel="shortcut icon">
    <link rel="stylesheet" type="text/css" href="${base}/curoAdmin/css/datatables/jquery.dataTables.min.css" />  

    <!-- Bootstrap core CSS -->
    <link href="${base}/assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="${base}/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        
    <!-- Custom styles for this template -->
    <link href="${base}/assets/css/style.css" rel="stylesheet">
    <link href="${base}/assets/css/style-responsive.css" rel="stylesheet">
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  
  <#include "public/index/top.ftl">
  <#include "public/index/left.ftl">
  <section id="main-content">
  <section class="wrapper">
  <div class="row">
	<div class="panel panel-white">
       <div class="panel-body">
   	   	   <#--<div class="form-group">
		   	   <div class="alert alert-theme alert-success fade in">
                	赞助须知：
                    <div class="content">
                    	1：赞助网站后，账户可升级为赞助用户权限。
                    	<br/>2：赞助用户权限具有申请远程部署服务特权。
                    	<br/>3：赞助用户权限具有更高的发布需求特权。
                    	<br/>4：如有疑问，可联系站长 <a target="_blank" href="https://wpa.qq.com/msgrd?v=3&uin=1181014088&site=qq&menu=yes"><img border="0" src="${base}/assets/icon_png/qq.gif" alt="联系站长" title="联系站长"/></a>
                    </div>
               </div>
           </div>
           <#if user?exists>
   	   	   <div class="form-group">
		   	   <div class="alert alert-theme alert-success fade in">
                    <div class="content">
	                	步骤一：选择赞助金额
                    	<select id="money" name="money" style="margin-left:5px;">   
		                    <option value="0" selected></option>   
		                    <option value="1">￥4.9</option>   
					        <option value="2">￥9.9</option>   
					        <option value="3">￥19.9</option>   
					        <option value="4">￥29.9</option>   
					        <option value="5">￥39.9</option>   
					        <option value="6">￥59.9</option>   
					        <option value="7">￥99.9</option>   
					    </select><br/><br/>
					    <button type="button" id="zanzhuButton" style="display:none;" class="btn btn-round btn-success">赞助</button>
                    </div>
               </div>
           </div>
	       </#if>    
   	   	   <div class="form-group" id="quanxian">
		   	   <div class="alert alert-theme alert-success fade in">
                    <div class="content">
                    	步骤二：升级赞助用户权限<br/><br/>
                    	<button type="button" id="shengjiButton" class="btn btn-round btn-success">升级账户权限</button>
                    </div>
               </div>
           </div>-->
           
           <div class="form-group">
		   	   		赞助网站用户，每隔10分钟更新一次。
		   	   <div class="alert alert-theme alert-success fade in">
                    <div class="content">
					        <div class="tab-content">
					            <div class="tab-pane fade in active" id="all">
					                <div class="list-group mail-list">
					                	<div class="panel panel-white">
						                    <div class="panel-body">
						                        <table id="table-1" class="table table-bordered table-striped">
						                            <thead>
						                                <th>序号</th>
						                                <th>用户</th>
						                                <th>操作</th>
						                                <th>金额</th>
						                                <th>时间</th>
						                            </thead>
						                            <tbody>
                  <#include "public/index/top/money/zanzhu.ftl">
							                    	</tbody>
								                </table>
								             </div>
							            </div>
							        </div>
							    </div>
							</div>
                    </div>
               </div>
           </div>
   		   
       </div>
    </div>
  </div>
  </section>
  </section>
  <#include "public/index/modal.ftl">
  
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="${base}/assets/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="${base}/assets/js/jquery.dcjqaccordion.2.7.js"></script>
  <script src="${base}/assets/js/jquery.scrollTo.min.js"></script>
  <script src="${base}/assets/js/jquery.nicescroll.js" type="text/javascript"></script>

  <!--common script for all pages-->
  <script src="${base}/assets/js/common-scripts.js"></script>
  
  <script src="${base}/curoAdmin/js/datatables/jquery.dataTables.min.js"></script>  
  <script src="${base}/curoAdmin/js/datatables-demo.js"></script> 
  
  <script type="text/javascript" src="${base}/star/lib/jquery.raty.min.js"></script>
  <script type="text/javascript"> 
  $(function(){
      $("select").bind("change",function () {  
    	var money = this.value;
        if (this.value== "0"){  
            $("#zanzhuButton").hide();
        }else{  
            $("#zanzhuButton").show();
        }
      });  
   }); 
	$("#zanzhuButton").bind("click",function(){
		var money = $("#money").val();
		var allData = {
			money:money
　　　　　 };
		if(confirm("确认赞助？")){
	        $.ajax({
	           url:'${base}/user/zanzhu',
	           type:'post',
	           contentType:'application/json;charset=UTF-8',
			   dataType:'json',
			   data:JSON.stringify(allData),
	           beforeSend: function () {
			        // 禁用按钮防止重复提交，发送前响应
			        $("#zanzhuButton").attr({ disabled: "disabled" });
	                $('#zanzhuButton').text("正在处理。。。");
			    },
	           success:function(data){
	           		alert(data.message);
	           		if(data.status==0){
	           			$('#zanzhuButton').removeAttr("disabled");
	           			$('#zanzhuButton').text("赞助");
	           		}else if(data.status==1){
	                    $('#zanzhuButton').text("赞助成功");
	           		}
	           },
	           error:function(){
					alert("服务器错误！请联系站长");
					$('#zanzhuButton').removeAttr("disabled");
					$('#zanzhuButton').text("赞助");
	           }
	         })
	    }
	});
	$("#shengjiButton").bind("click",function(){
        $.ajax({
           url:'${base}/user/shengji',
           type:'post',
           contentType:'application/json;charset=UTF-8',
		   dataType:'json',
           beforeSend: function () {
		        // 禁用按钮防止重复提交，发送前响应
		        $("#shengjiButton").attr({ disabled: "disabled" });
                $('#shengjiButton').text("正在处理。。。");
		    },
           success:function(data){
           		alert(data.message);
           		if(data.status==0){
           			$('#shengjiButton').removeAttr("disabled");
           			$('#shengjiButton').text("升级账户权限");
           		}else if(data.status==1){
                    $('#shengjiButton').text("升级权限成功");
           		}
           },
           error:function(){
				alert("服务器错误！请联系站长");
				$('#shengjiButton').removeAttr("disabled");
				$('#shengjiButton').text("升级账户权限");
           }
         })
	});
  </script>
  </body>
</html>
