<#assign base=request.contextPath />
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="蜗牛库 - 解答咨询问题。">
    <meta name="author" content="蜗牛库">
    <meta name="keyword" content="源码, 免费下载, 在线演示, 技术支持, 咨询服务">

    <title>解答咨询问题  - 蜗牛库</title>
    <link href="${base}/assets/img/logo.ico" rel="shortcut icon">

    <!-- Bootstrap core CSS -->
    <link href="${base}/assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="${base}/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        
    <!-- Custom styles for this template -->
    <link href="${base}/assets/css/style.css" rel="stylesheet">
    <link href="${base}/assets/css/style-responsive.css" rel="stylesheet">
    
    <link rel="stylesheet" type="text/css" href="${base}/umeditor/themes/default/css/umeditor.css" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  
  <#include "public/index/top.ftl">
  <#include "public/index/left.ftl">
  <#include "public/question/main.ftl">
  <#include "public/index/modal.ftl">
  
  <script src="${base}/umeditor/third-party/jquery.min.js"></script>
  <script src="${base}/umeditor/umeditor.config.js"></script>
  <script src="${base}/umeditor/umeditor.min.js"></script>
  <script src="${base}/umeditor/lang/zh-cn/zh-cn.js"></script>
  
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="${base}/assets/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="${base}/assets/js/jquery.dcjqaccordion.2.7.js"></script>
  <script src="${base}/assets/js/jquery.scrollTo.min.js"></script>
  <script src="${base}/assets/js/jquery.nicescroll.js" type="text/javascript"></script>

  <!--common script for all pages-->
  <script src="${base}/assets/js/common-scripts.js"></script>
  <script>
    	$(document).ready(function(){
			var id = $("#id").val();
			var allData = {
　　　　　　　　　　  	id:id
　　　　　　　　   };
	        $.ajax({
               url:'${base}/resource/getQuestionById',
               type:'post',
               contentType:'application/json;charset=UTF-8',
			   dataType:'json',
               data:JSON.stringify(allData),
               success:function(data){
               		if(data.status==0){
           				alert(data.message);
               		}else if(data.status==1){
	                    $("#money").html(data.money);
	                    $("#title").html(data.title);
	                    $("#resource").html(data.resource);
               		}
               },
               error:function(){
					alert("服务器错误！请联系站长");
               }
	         });
		});
		var um = UM.getEditor('jieda');
		function getContent() {
	        var arr = [];
	        arr.push(UM.getEditor('jieda').getContent());
	        if(arr==""){
	          alert("请输入内容！");
	          UM.getEditor('jieda').focus();
			  return false;
	        }
	        var content = arr.join("\n");
	        var id = $("#id").val();
	        var allData = {
　　　　　　　　　　  	id:id,
				content:content
　　　　　　　　   };
	        $.ajax({
               url:'${base}/resource/answer',
               type:'post',
               contentType:'application/json;charset=UTF-8',
			   dataType:'json',
               data:JSON.stringify(allData),
               beforeSend: function () {
			        // 禁用按钮防止重复提交，发送前响应
			        $("#sendAnswerButton").attr({ disabled: "disabled" });
                    $('#sendAnswerButton').text("正在将答案通过邮件发送给提问者。。。");
			    },
               success:function(data){
	               	alert(data.message);
               		if(data.status==0){
               			$('#sendAnswerButton').removeAttr("disabled");
	                    $('#sendAnswerButton').text("发布答案");
               		}else if(data.status==1){
               			window.location.href = "${base}/resource/"+data.id+"_new.html";
               		}
               },
               error:function(){
					alert("服务器错误！请联系站长");
					$('#sendAnswerButton').removeAttr("disabled");
               }
	         });
		}	
  </script>
  </body>
</html>
