<#assign base=request.contextPath />
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="蜗牛库 - 找到您需要的源码项目，在线演示和免费下载。">
    <meta name="author" content="蜗牛库">
    <meta name="keyword" content="源码, 免费下载, 在线演示, 技术支持, 咨询服务">

    <title>蜗牛库 - 找到您需要的源码项目，在线演示和免费下载。</title>
    <link href="${base}/assets/img/logo.ico" rel="shortcut icon">

    <!-- Bootstrap core CSS -->
    <link href="${base}/assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="${base}/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        
    <!-- Custom styles for this template -->
    <link href="${base}/assets/css/style.css" rel="stylesheet">
    <link href="${base}/assets/css/style-responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <#include "public/index/top.ftl">
  <div id="login-page">
  	<div class="container">
  	
      <div class="form-login"> 
        <h2 class="form-login-heading">登录</h2>
        <div class="login-wrap">
            <input type="text" class="form-control" name="email" id="email" placeholder="邮箱" autofocus>
            <br>
            <input type="password" name="password" id="password" class="form-control" placeholder="密码">
            <label class="checkbox">
                <span class="pull-right">
                    <a data-toggle="modal" href="login.html#myModal"> 忘记密码？</a>
                </span>
            </label>
            <button class="btn btn-theme btn-block" id="loginButton" type="submit"><i class="fa fa-lock"></i> 登录</button>
            <hr>
            
            <div class="registration">
               	 没有账号？
                <a href="${base}/zhuce.html"> 注册</a>
            </div>

        </div>
      </div>

      <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
          <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                      <h4 class="modal-title">忘记密码 ?</h4>
                  </div>
                  <div class="modal-body">
                      <p>通过电子邮件重置密码.</p>
                      <input type="text" id="email1" placeholder="邮箱" autocomplete="off" class="form-control placeholder-no-fix">
                  </div>
                  <div class="modal-footer">
                      <button class="btn btn-theme" id="updatePassword" type="button">确定</button>
                  </div>
              </div>
          </div>
      </div>
          
  	</div>
  </div>
	  
    <!-- js placed at the end of the document so the pages load faster -->
    <script src="${base}/assets/js/jquery.js"></script>
    <script src="${base}/assets/js/bootstrap.min.js"></script>
    <script src="${base}/assets/js/autoMail.1.0.min.js"></script>

    <!--BACKSTRETCH-->
    <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
    <script type="text/javascript" src="${base}/assets/js/jquery.backstretch.min.js"></script>
    <script>
    	$(document).ready(function(){
			$('#email,#email1').autoMail({
				emails:['qq.com','163.com','126.com','sina.com','sohu.com']
			});
		});
        //$.backstretch("背景图片路径", {speed: 500});
        $(document).ready(function(){
			$("#loginButton").bind("click",function(){
                var email = $('#email').val();
                var password = $('#password').val();
                if(email==""){
				    alert("请输入邮箱！");
				    $('#email').focus();
				    return false;
				}
				if(password==""){
					alert("请输入密码！");
				    $('#password').focus();
				    return false;
				}
				var allData = {
	　　　　　　　　　　  email:email,
               		password:password,
	　　　　　　　　};
		        $.ajax({
	               url:'${base}/user/login',
	               type:'post',
	               contentType:'application/json;charset=UTF-8',
				   dataType:'json',
	               data:JSON.stringify(allData),
	               success:function(data){
	               		if(data.status==0){
		               		alert(data.message);
	               		}else if(data.status==1){
		                    window.location.href = "${base}/index.html";
	               		}
	               },
	               error:function(){
						alert("服务器错误！请联系站长");
	               }
		         });
            });
            $("#updatePassword").bind("click",function(){
                var email1 = $('#email1').val();
		        $.ajax({
	               url:'${base}/email/updatePassword',
	               type:'post',
	               contentType:'application/json;charset=UTF-8',
				   dataType:'json',
	               data:email1,
	               beforeSend: function () {
				        // 禁用按钮防止重复提交，发送前响应
				        $("#updatePassword").attr({ disabled: "disabled" });
	                    $('#updatePassword').text("邮件正在发送。。。");
				    },
	               success:function(data){
	               		alert(data.message);
	               		if(data.status==0){
	               			$('#updatePassword').removeAttr("disabled");
	               		}else if(data.status==1){
		                    $('#updatePassword').text("邮件发送成功");
	               		}
	               },
	               error:function(){
						alert("服务器错误！请联系站长");
						$('#updatePassword').removeAttr("disabled");
	               }
		         });
            });
		});
    </script>

  </body>
</html>
