<#assign base=request.contextPath />
<div class="header-fixed skin-blue">
<aside class="content-wrapper collapse sidebarLeft">
<div class="content container-fluid sidebarRight animated fadeInUp mail message-list-wrapper"> 
<div class="panel panel-white">
<div class="panel-body"> 
<div class="row">

	<#include "left.ftl">
    
    <div class="col-sm-9 col-md-10" id="demandContent">
        <div class="tab-content">
            <div class="tab-pane fade in active" id="all">
                <div class="list-group mail-list">
                	<div class="panel panel-white">
	                    <div class="panel-body">
	                        <table id="table-1" class="table table-bordered table-striped">
	                            <thead>
	                            	<th>序号</th>
	                                <th>用户</th>
	                                <th>状态</th>
	                                <th>标题</th>
	                                <th>时间</th>
	                            </thead>
	                            <tbody>
	                                <#include "main/completeDemands.ftl">
	                            </tbody>
	                        </table>
	                    </div>
                    </div>
                    
                </div>
            </div>
            
        </div>
    </div>
</div>
</div> 
</div>
</div>
</aside>
</div>
