<#assign base=request.contextPath />
<div class="header-fixed skin-blue">
<aside class="content-wrapper collapse sidebarLeft">
<div class="content container-fluid sidebarRight animated fadeInUp mail message-list-wrapper"> 
<div class="panel panel-white">
<div class="panel-body"> 
<div class="row">
    <div class="col-sm-3 col-md-2 mg-btm-30">
    	<a href="compose.html" class="btn btn-blue btn-sm btn-block">在线演示</a>
        <a href="compose.html" class="btn btn-green btn-sm btn-block">免费下载</a>
        <hr style="margin-top: 20px;margin-bottom: 20px;border: 0;border-top: 1px solid #eee;"/>
        <a href="compose.html" class="btn btn-danger btn-sm btn-block">问题咨询</a>
        <a href="compose.html" class="btn btn-purple btn-sm btn-block">远程部署</a>
    	<#include "main/soft.ftl">
    </div>
    <div class="col-sm-9 col-md-10">
        <div class="attatchments-wrapper">
        	<div class="panel panel-white">
				<div class="panel-body"> 
		            <h3 class="subject">
		        	<img src="${base}/assets/icon_png/sousuo_small.png" style="margin-right:5px;margin-bottom:5px;"/>
		                                基于struts+spring+spring jdbc实现的代码分享网
		                <button type="button" class="btn btn-default pull-right" data-toggle="dropdown">
		                    <span class="star">
		                        <i class="fa fa-star-o"></i><i class="fa fa-star color-light-orange"></i>喜欢
		                    </span>
		                </button> 
		            </h3>
		            <hr style="margin-top: 20px;margin-bottom: 20px;border: 0;border-top: 1px solid #eee;"/>
		            <div class="content">
		                <div class="message">
		                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, dolor sit amet, consectetuer adipiscing elit, consectetuer adipiscing elit, sed dia nonum, consectetuer adipiscing elit, sed dia nonum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, dolor sit amet, consectetuer adipiscing elit, consectetuer adipiscing elit, sed dia nonum, consectetuer adipiscing elit, sed dia nonum.</p>
		                    <ul>
		                        <li>List Item 1</li>
		                        <li>List Item 2</li>
		                        <li>List Item 3</li>
		                        <li>List Item 4</li>
		                    </ul>
		                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, dolor sit amet, consectetuer adipiscing elit, consectetuer adipiscing elit, sed dia nonum, consectetuer adipiscing elit, sed dia nonum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, dolor sit amet, consectetuer adipiscing elit, consectetuer adipiscing elit, sed dia nonum, consectetuer adipiscing elit, sed dia nonum.</p>
		                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, dolor sit amet, consectetuer adipiscing elit, consectetuer adipiscing elit, sed dia nonum ctetuer adipiscing elit, sed dia nonum, consectetuer adipiscing elit, sed dia nonum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, dolor sit amet, consectetuer adipiscing elit, consectetuer adipiscing elit, sed dia nonum, consectetuer adipiscing elit, sed dia nonum.</p>
		                </div>
		            </div>
		        </div>
		    </div>
        	<div class="panel panel-white border-top-orange">
                <div class="panel-heading">
                    <h3 class="panel-title">问题解答</h3>
                </div>
                <div class="panel-body">
                    <div class="panel-group" id="accordion">
                        
                        <div class="panel">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                        <span class="badge bg-light-green squared" style="margin-right:10px;">已回答</span>#1 站长
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                </div>
                            </div>
                        </div>
                        
                        <div class="panel">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                        <span class="badge bg-light-blue squared" style="margin-right:10px;">待回答</span>#2 站长
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse in">
                                <div class="panel-body">
                                	<ul class="nav nav-pills nav-stacked attatchments">
                                        <li>
                                            <a href="#"> 
                                                <span class="pull-right">8.5MB</span>
                                                <span class="badge bg-light-blue squared">DOC</span> project53.doc
                                            </a>
                                        </li> 
                                        <li>
                                            <a href="#"> 
                                                <span class="pull-right">238.5MB</span>
                                                <span class="badge bg-light-green squared">ZIP</span> project53.zip
                                            </a>
                                        </li> 
                                        <li>
                                            <a href="#"> 
                                                <span class="pull-right">12.5MB</span>
                                                <span class="badge bg-light-orange squared">PSD</span> </i> flyer.psd
                                            </a>
                                        </li> 
                                        <li>
                                            <a href="#"> 
                                                <span class="pull-right">1.2 GB</span>
                                                <span class="badge bg-light-purple squared">MP4</span> </i> promo_vid.mp4
                                            </a>
                                        </li> 
                                    </ul>  
                                </div>
                            </div>
                        </div>
                        
                        <div class="panel">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                        <span class="badge bg-light-orange squared" style="margin-right:10px;">未回答</span>#3 站长
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                </div>
                            </div>
                        </div>
                        
                   </div>
               </div>
               
           </div>
            
   		</div>  
	</div>
	
</div> 
</div> 
</div>
</div>
</aside>
</div>