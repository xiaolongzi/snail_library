<#assign base=request.contextPath />
<section id="main-content">
  <section class="wrapper">
      <div class="row">
          <div class="col-lg-9 main-chart">
          	<#include "right/main.ftl">
    		<#include "right/main/yuanma.ftl">
          </div>
          <div class="col-lg-3 ds">
    		<#include "right/main/yanshi.ftl">
    		<#include "right/main/xiazai.ftl">
    		<#include "right/main/biaoqian.ftl">
          </div>
      </div>
  </section>
</section>