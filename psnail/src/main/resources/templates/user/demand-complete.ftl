<#assign base=request.contextPath />
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="发现源码 - 发现您需要的源码，在线演示和免费下载。">
    <meta name="author" content="蜗牛库">
    <meta name="keyword" content="搜索源码, 免费下载, 在线演示, 技术支持, 咨询服务">

    <title>发布需求 - 蜗牛库</title>
    <link href="${base}/assets/img/logo.ico" rel="shortcut icon">

     <!-- Bootstrap core CSS -->
    <link href="${base}/assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="${base}/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="${base}/curoAdmin/css/datatables/jquery.dataTables.min.css" />  
    
    <!-- Custom styles for this template -->
    <link href="${base}/assets/css/style.css" rel="stylesheet">
    <link href="${base}/assets/css/style-responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="${base}/assets/css/to-do.css">
    
    <link rel="stylesheet" type="text/css" href="${base}/curoAdmin/css/style.css" /> 
    
    <link rel="stylesheet" type="text/css" href="${base}/umeditor/themes/default/css/umeditor.css" /> 

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
      <#include "public/index/top.ftl">
      <#include "public/index/left.ftl">
      <#include "public/demand/complete.ftl">
      <#include "public/index/modal.ftl">
  </body>
  
  <!-- js placed at the end of the document so the pages load faster -->
  
  <script src="${base}/umeditor/third-party/jquery.min.js"></script>
  <script src="${base}/umeditor/umeditor.config.js"></script>
  <script src="${base}/umeditor/umeditor.min.js"></script>
  <script src="${base}/umeditor/lang/zh-cn/zh-cn.js"></script>
  
  <script src="${base}/assets/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="${base}/assets/js/jquery.dcjqaccordion.2.7.js"></script>
  <script src="${base}/assets/js/jquery.scrollTo.min.js"></script>
  <script src="${base}/assets/js/jquery.nicescroll.js" type="text/javascript"></script>

  <!--common script for all pages-->
  <script src="${base}/assets/js/common-scripts.js"></script>
  
  <script src="${base}/curoAdmin/js/datatables/jquery.dataTables.min.js"></script>  
  <script src="${base}/curoAdmin/js/datatables-demo.js"></script> 
  
  <script type="text/javascript">
	  //实例化编辑器
      var um = UM.getEditor('myDemand');
      function sendDemand() {
        var title=$("#title").val();
	  	if(title==""){
		  alert("请输入标题！");
		  $('#title').focus();
		  return false;
		}
        var arr = [];
        arr.push(UM.getEditor('myDemand').getContent());
        if(arr==""){
          alert("请输入内容！");
          UM.getEditor('myDemand').focus();
		  return false;
        }
        var content = arr.join("\n");
        var allData = {
　　　　　　　 title:title,
           content:content
　　　　　 };
        $.ajax({
           url:'${base}/user/sendDemand',
           type:'post',
           contentType:'application/json;charset=UTF-8',
		   dataType:'json',
           data:JSON.stringify(allData),
           success:function(data){
           		if(data.status==1){
           			alert("发布需求成功！您的需求将会在5分钟内加入到需求列表中，请耐心等待。");
                    window.location.href = "${base}/demand.html";
           		}else{
	           		alert(data.message);
           		}
           },
           error:function(){
				alert("服务器错误！请联系站长");
           }
        })
      }
  </script> 
  
</html>