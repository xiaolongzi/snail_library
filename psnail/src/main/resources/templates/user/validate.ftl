<#assign base=request.contextPath />
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="蜗牛库 - 解答咨询问题。">
    <meta name="author" content="蜗牛库">
    <meta name="keyword" content="源码, 免费下载, 在线演示, 技术支持, 咨询服务">

    <title>验证解决方案  - 蜗牛库</title>
    <link href="${base}/assets/img/logo.ico" rel="shortcut icon">

    <!-- Bootstrap core CSS -->
    <link href="${base}/assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="${base}/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        
    <!-- Custom styles for this template -->
    <link href="${base}/assets/css/style.css" rel="stylesheet">
    <link href="${base}/assets/css/style-responsive.css" rel="stylesheet">
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  
  <#include "public/index/top.ftl">
  <#include "public/index/left.ftl">
  <section id="main-content">
  <section class="wrapper">
  <div class="row">
	<div class="panel panel-white">
       <div class="panel-body">
   	   	   <div class="form-group">
		   	   <div class="alert alert-theme alert-success fade in">
                	注意事项：
                    <div class="content">
                    	1：请先登录，只有登录后才可以操作
                    	<br/>2：请验证解决方案是否能够解决您的问题，感谢您的支持！
                    	<br/>3：若您存在疑问，请联系站长  <a target="_blank" href="https://wpa.qq.com/msgrd?v=3&uin=1181014088&site=qq&menu=yes"><img border="0" src="${base}/assets/icon_png/qq.gif" alt="联系站长" title="联系站长"/></a>
                    </div>
               </div>
           </div>
           <div class="form-group">
		   	   <div class="alert alert-theme alert-success fade in">
		   	   		<input type="hidden" id="id" value="${RequestParameters["id"]}"/>
		   	   		您的需求：<span id="title"></span>
		   	   		<br/><br/>解决方案：<span id="content"></span>
		   	   		<br/><br/>
		   	   		此解决方案是否解决您的需求？
		   	   		<button type="button" onclick="validate(3)" class="btn btn-round btn-success">已解决</button>
		   	   		<button type="button" onclick="validate(2)" class="btn btn-round btn-danger">未解决</button>
               </div>
           </div>
   		   
       </div>
    </div>
  </div>
  </section>
  </section>
  <#include "public/index/modal.ftl">
  
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="${base}/assets/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="${base}/assets/js/jquery.dcjqaccordion.2.7.js"></script>
  <script src="${base}/assets/js/jquery.scrollTo.min.js"></script>
  <script src="${base}/assets/js/jquery.nicescroll.js" type="text/javascript"></script>

  <!--common script for all pages-->
  <script src="${base}/assets/js/common-scripts.js"></script>
  
  <script type="text/javascript" src="${base}/star/lib/jquery.raty.min.js"></script>
  <script>
    	$(document).ready(function(){
			var id = $("#id").val();
			var allData = {
　　　　　　　　　　  	id:id
　　　　　　　　   };
	        $.ajax({
               url:'${base}/demand/getAnswerAndDemand',
               type:'post',
               contentType:'application/json;charset=UTF-8',
			   dataType:'json',
               data:JSON.stringify(allData),
               success:function(data){
               		if(data.status==0){
           				alert(data.message);
               		}else if(data.status==1){
	                    $("#title").html(data.title);
	                    $("#content").html(data.content);
               		}
               },
               error:function(){
					alert("服务器错误！请联系站长");
               }
	         });
	         
		});
		function validate(state) {
		var msg;
		if(state==2){
			msg="确定未解决？";
		}else{
			msg="确定已解决？"
		}
		if(confirm(msg)){
			var id = $("#id").val();
	        var allData = {
　　　　　　　　　　  	id:id,
				state:state
　　　　　　　　   };
	        $.ajax({
               url:'${base}/demand/validate',
               type:'post',
               contentType:'application/json;charset=UTF-8',
			   dataType:'json',
               data:JSON.stringify(allData),
               success:function(data){
       				alert(data.message);
               		if(data.status==1){
	                    window.location.href = "${base}/demand/"+data.demand_id+"_new.html";
               		}
               },
               error:function(){
					alert("服务器错误！请联系站长");
               }
	         });
		}
	    }
  </script>
  </body>
</html>
