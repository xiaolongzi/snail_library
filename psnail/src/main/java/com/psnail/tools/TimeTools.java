package com.psnail.tools;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeTools {
	/**
	 * 该方法将时间戳改成几天前、几小时前、几分钟前
	 * 
	 * @param date
	 * @return
	 */
	public static String getTimeFormatText(Date date) {
		long minute = 60 * 1000;// 1分钟
		long hour = 60 * minute;// 1小时
		long day = 24 * hour;// 1天
		long month = 31 * day;// 月
		long year = 12 * month;// 年
		if (date == null) {
			return null;
		}
		long diff = new Date().getTime() - date.getTime();
		long r = 0;
		if (diff > year) {
			/*
			 * r = (diff / year); return r + "年前";
			 */
			return new SimpleDateFormat("yyyy-MM-dd").format(date);
		}
		if (diff > month) {
			/*
			 * r = (diff / month); return r + "月前";
			 */
			return new SimpleDateFormat("yyyy-MM-dd").format(date);
		}
		if (diff > day) {
			/*
			 * r = (diff / day); return r + "天前";
			 */
			return new SimpleDateFormat("yyyy-MM-dd").format(date);
		}
		if (diff > hour) {
			r = (diff / hour);
			return r + "小时前";
		}
		if (diff > minute) {
			r = (diff / minute);
			return r + "分钟前";
		}
		return "刚刚";
	}
}
