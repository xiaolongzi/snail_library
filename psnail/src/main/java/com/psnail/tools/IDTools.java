package com.psnail.tools;

import java.util.UUID;

public class IDTools {
	public static String createId() {
		return UUID.randomUUID().toString().trim().replaceAll("-", "");
	}
}
