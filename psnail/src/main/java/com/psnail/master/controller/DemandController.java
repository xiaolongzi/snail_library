package com.psnail.master.controller;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.psnail.master.service.DemandService;
import com.psnail.tools.GsonTools;
import com.psnail.tools.PathTools;
import com.psnail.tools.TextFileTools;

@Controller
@RequestMapping("/demand")
public class DemandController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private DemandService demandService;

	/**
	 * 文件不存在，自动生成需求详情页
	 */
	@RequestMapping(value = "/{id}.html", method = RequestMethod.GET)
	public String html(@PathVariable("id") String id) {
		String filePath = PathTools.getPath() + File.separator + "templates"
				+ File.separator + "demand" + File.separator + id + ".ftl";
		File file = new File(filePath);
		if (!file.exists()) {
			try {
				file.createNewFile();
				TextFileTools.write(filePath, false,
						demandService.detail2Html(id));
			} catch (IOException e) {
				logger.info("需求详情界面文件创建失败，路径：" + filePath);
			}
		}
		return "demand/" + id;
	}

	/**
	 * 文件存在，更新需求详情页
	 */
	@RequestMapping(value = "/{id}_new.html", method = RequestMethod.GET)
	public String html_new(@PathVariable("id") String id) {
		String filePath = PathTools.getPath() + File.separator + "templates"
				+ File.separator + "demand" + File.separator + id + ".ftl";
		File file = new File(filePath);
		if (!file.exists()) {
			try {
				file.createNewFile();
				TextFileTools.write(filePath, false,
						demandService.detail2Html(id));
			} catch (IOException e) {
				logger.info("需求详情界面文件创建失败，路径：" + filePath);
			}
		} else {
			TextFileTools.write(filePath, false, demandService.detail2Html(id));
		}
		return "demand/" + id;
	}

	/**
	 * 关注需求
	 */
	@RequestMapping(value = "/guanzhu", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String guanzhu(@RequestBody Map<String, Object> reqMap) {
		String id = reqMap.get("id").toString();
		logger.info("resource/guanzhu()接收的参数id：" + id);
		Map<String, Object> result = demandService.guanzhu(id);
		logger.info("resource/guanzhu()返回结果集：" + result);
		return GsonTools.toJson(result);
	}

	/**
	 * 需求发布新的解决方案
	 */
	@RequestMapping(value = "/answer", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String answer(@RequestBody Map<String, Object> reqMap) {
		String id = reqMap.get("id").toString();
		String content = reqMap.get("content").toString();
		logger.info("resource/answer()接收的参数id：" + id + "，content：" + content);
		Map<String, Object> result = demandService.answer(id, content);
		logger.info("resource/answer()返回结果集：" + result);
		return GsonTools.toJson(result);
	}

	/**
	 * 获得解决方案的内容及需求内容
	 */
	@RequestMapping(value = "/getAnswerAndDemand", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String getAnswerAndDemand(@RequestBody Map<String, Object> reqMap) {
		String id = reqMap.get("id").toString();
		logger.info("resource/getAnswerAndDemand()接收的参数id：" + id);
		Map<String, Object> result = demandService.getAnswerAndDemand(id);
		logger.info("resource/getAnswerAndDemand()返回结果集：" + result);
		return GsonTools.toJson(result);
	}

	/**
	 * 验证解决方案是否解决
	 */
	@RequestMapping(value = "/validate", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String validate(@RequestBody Map<String, Object> reqMap) {
		String id = reqMap.get("id").toString();
		String state = reqMap.get("state").toString();
		logger.info("resource/validate()接收的参数id：" + id + "，state：" + state);
		Map<String, Object> result = demandService.validateAnswer(id, state);
		logger.info("resource/validate()返回结果集：" + result);
		return GsonTools.toJson(result);
	}
}