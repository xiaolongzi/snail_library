package com.psnail.master.mapper;

import com.psnail.master.entity.FocusD;
import com.psnail.master.entity.FocusDExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface FocusDMapper {
    int countByExample(FocusDExample example);

    int deleteByExample(FocusDExample example);

    int insert(FocusD record);

    int insertSelective(FocusD record);

    List<FocusD> selectByExample(FocusDExample example);

    int updateByExampleSelective(@Param("record") FocusD record, @Param("example") FocusDExample example);

    int updateByExample(@Param("record") FocusD record, @Param("example") FocusDExample example);
}