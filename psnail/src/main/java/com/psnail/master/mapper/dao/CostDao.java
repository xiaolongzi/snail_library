package com.psnail.master.mapper.dao;

import java.math.BigDecimal;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.psnail.master.entity.dto.CostList;

public interface CostDao {

	BigDecimal sumByWay(@Param("way") int way);

	List<CostList> getCostListByWay3();

}