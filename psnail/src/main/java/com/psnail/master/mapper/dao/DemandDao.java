package com.psnail.master.mapper.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.psnail.master.entity.dto.Demands;

public interface DemandDao {

	int countByAll();

	int countMonthByUser_id(String user_id);

	List<Demands> getAllDemands();

	List<Demands> getAllDemandsByVip();

	List<Demands> getAllDemandsByZanZhu();

	List<Demands> getAllDemandsByState(@Param("state") int state);
}