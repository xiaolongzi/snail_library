package com.psnail.master.service;

import java.util.Map;

public interface DemandService {

	Map<String, Object> guanzhu(String id);

	Map<String, Object> answer(String id, String content);

	String detail2Html(String id);

	Map<String, Object> getAnswerAndDemand(String id);

	Map<String, Object> validateAnswer(String id, String state);

}
