package com.psnail.master.entity.dto;

public class BiaoQian {
	private String id;
	private String title;
	private String name;
	private String photo;
	private String background;

	public String getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getName() {
		return name;
	}

	public String getPhoto() {
		return photo;
	}

	public String getBackground() {
		return background;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public void setBackground(String background) {
		this.background = background;
	}

}
